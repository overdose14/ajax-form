/*
 * PHP form ajax plugin
 * Dependencies: jQuery (tested up version v1.10.1)
 * Usage: replace ID with ID of contact form which needs to be AJAX'ed, replace url: with your contact form URL and lastly replace thankyou.html with URL of your thankyou page
 * Written by: Abhijeet Sidhu
 * URL: http://od14.com
 * Contact/Feedback/Concerns: abhijeet.sidhu@gmail.com
*/

var contactFormID   = $('#ID');
var contactFormURL  = 'Contact form URL';
var thankyoupageURL = 'URL';

$(contactFormID)
  .on("submit",function(e) {
    e.preventDefault();
      $.ajax({
        type: 'POST',
        url: contactFormURL,
        data: contactFormID.serialize(),
        success: function(data,textStatus) {
          if (textStatus=="success" && data.length > 1) {
            alert(data);
          } else if (textStatus=="success" && data.length==1) {
            window.location.replace(thankyoupageURL);
          }
        }
      })
  });