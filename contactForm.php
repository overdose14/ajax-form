<?php

require_once('recaptchalib.php');
require_once('Akismet.class.php');

$privatekey = "6LeVStMSAAAAABz5JxhYsZnY273LL9ztSfPJv3Vy";
$publickey = "6LeVStMSAAAAAMexgFYck4AdBTivaB1tm8Egkdvj";

$sendto = 'id@prodiver.com';
$name = clean_var($_POST['name']);
$email = clean_var($_POST['email']);
$sendfrom_admin = "$name <".$name.">";
$sendfrom = 'Sender <info@sender.com>';
$auto_res = $email;

$subject = 'Subject for autoresponder';
$errormessage = 'There was an error in:';

$alert = '';
$pass = 0;

//SELECT
if ( !isset($_POST['type']) ) {
	$pass = 1;
	$alert .= "Please select a type from dropdown\n";
}

//INPUT. change 'Your Name' to placeholder
if ( empty($name) || ($name == 'Your Name' )) {
	$pass = 1;
	$alert .= "Please enter your Name\n";
} elseif ( preg_match( "/[{}()*+?.\\^$|]/", $name ) ) {
	$pass = 1;
	$alert .=  "Please enter correct Name\n";
}

//Incase name is borken into first name and last name
if ( empty($fname) || ($fname == 'Your First Name' )) {
	$pass = 1;
	$alert .= "Please enter your First name\n";
} elseif ( preg_match( "/[{}()*+?.\\^$|]/", $fname ) ) {
	$pass = 1;
	$alert .=  "Please enter correct First name\n";
}

if ( empty($lname) || ($lname == 'Your Last Name' )) {
	$pass = 1;
	$alert .= "Please enter your Last name\n";
} elseif ( preg_match( "/[{}()*+?.\\^$|]/", $lname ) ) {
	$pass = 1;
	$alert .=  "Please enter correct Last name\n";
}

//INPUT
if ( empty($email) || ($email == 'Email Address')) {
	$pass = 1;
	$alert .= "Please enter your Email\n";
} elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	$pass = 1;
	$alert .= "Please enter a correct email\n";
}

//INPUT
if(empty($_REQUEST['phone']) || ($_POST['phone'] == 'Phone')) {
			$pass = 1;
			$alert .= "Please enter your Phone Number\n";
}

//INPUT
if ( empty($_POST['city']) || ($_POST['city'] == 'City')) {
	$pass = 1;
	$alert .= "Please enter your City\n";
}

$akismet = new Akismet('http://www.site.com/contactFormURL', '6b7010ad3c15');
$akismet->setCommentAuthor($name);
$akismet->setCommentAuthorEmail($email);
// $akismet->setCommentAuthorURL($url);
$akismet->setCommentContent(clean_var($_POST['type'])).', '.clean_var($_POST['info']);
$akismet->setPermalink('http://www.site.com/contactFormURL/');

if(!$akismet->isCommentSpam())
{
	$resp = recaptcha_check_answer ($privatekey,$_SERVER["REMOTE_ADDR"],$_POST["recaptcha_challenge_field"],$_POST["recaptcha_response_field"]);

	if (!$resp->is_valid) {
	  $pass = 1;
	  $captcha_e = "Please enter the correct Captcha";
	  $alert .= $captcha_e."\n";
	}

	if ( $pass==1 ) {
		echo $alert;
		} else {
			// $message = "Full Name: ".			clean_var($_POST['fname']).' '.clean_var($_POST['lname'])."\n";
			// $message .= "Company: ".			clean_var($_POST['company'])."\n";

			$message  = "Type: ". 				clean_var($_POST['type'])."\n";
			$message .= "Full Name: ".			clean_var($_POST['name'])."\n";
			$message .= "Email: ".				clean_var($_POST['email'])."\n";
			$message .= "Telephone: ".			clean_var($_POST['phone'])."\n";
			$message .= "City: ".				clean_var($_POST['city'])."\n";
			$message .= "Additional Info: ".	clean_var($_POST['info']);

			//Header for admin notification
			$header	= 'From: '.$sendfrom_admin."\r\n";
			$header .= 'Cc: seo@site.com'."\r\n";

			//Header for autoresponder
			$header2 = "From: Site <".$sendfrom.">\r\n";

			//Auto Responder
			mail($auto_res,"Thank you for submitting a Request","Dear ".$name.",\n\nThank you for submitting a form. An site team member will contact you shortly to follow-up your request.\n\nBest Regards,\n\nSite",$header2);
			//mail($auto_res,"Thank you for submitting a Request","Dear ".$fname.' '.$lname.",\n\nThank you for submitting a form. An site team member will contact you shortly to follow-up your request.\n\nBest Regards,\n\nSite",$header2);

			//Admin notification
			mail($sendto, $subject, $message, $header);
	}
}

function clean_var($variable) {
		$variable = strip_tags(stripslashes(trim(rtrim($variable))));
	  return $variable;
	}

?>

